import { ExcelService } from './../services/excel/excel.service';
import { Component, OnInit } from '@angular/core';
import { list } from 'cart-localstorage';

@Component({
  selector: 'app-lsdata',
  templateUrl: './lsdata.page.html',
  styleUrls: ['./lsdata.page.scss'],
})
export class LsdataPage implements OnInit {

  visitorLog:any = [];
  constructor(
    private data: ExcelService
  ) { }

  ngOnInit() {
    this.visitorLog =  list();
    console.log(JSON.stringify(this.visitorLog));
  }

  exportToExcel() {
    this.data.exportAsExcelFile(this.visitorLog, 'VistorsLogExcel');
  }



}
