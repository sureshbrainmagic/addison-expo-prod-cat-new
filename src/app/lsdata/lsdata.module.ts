import { ExcelService } from './../services/excel/excel.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LsdataPageRoutingModule } from './lsdata-routing.module';
import { LsdataPage } from './lsdata.page';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LsdataPageRoutingModule
  ],
  providers:[
    ExcelService
  ],
  declarations: [LsdataPage]
})
export class LsdataPageModule {}
