import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LsdataPage } from './lsdata.page';

const routes: Routes = [
  {
    path: '',
    component: LsdataPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LsdataPageRoutingModule {}
