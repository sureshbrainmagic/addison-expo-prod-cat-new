import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ViewpdfComponent } from 'src/app/component/viewpdf/viewpdf.component';


@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.page.html',
  styleUrls: ['./productlist.page.scss'],
})
export class ProductlistPage implements OnInit {

  obj: any = {};
  productsJSON: any = [
    {
      img: '../../../assets/icon/Drills.png',
      name: 'Drills',
      code: 'HoleMaking',
      video : '../../../assets/videos/drill.mp4',
      share : true,
      ProductSubCategory	: 'PSTD,TSTD and Centre Drills',
      localpdf: '../../../assets/pdf/Twistdrillcatalogue.pdf',
      onlinepdf: 'https://ecatalogue.addisonreach.co.in/Admin/brochurenew/Twistdrillcatalogue.pdf'
    },
    {
      img: '../../../assets/icon/Reamers.png',
      name: 'Reamers',
      code: 'HoleMaking',
      video : '../../../assets/videos/drill.mp4',
      share : true,
      ProductSubCategory	: 'Hand Reamer,Machine Reamer,Chuckling Reamer,Shell Reamer,Pin Reamer,Counter Bore,Counter Sink',
      localpdf: '../../../assets/pdf/GroundThreadTapscatalogue195-231.pdf',
      onlinepdf: 'https://ecatalogue.addisonreach.co.in/Admin/brochurenew/GroundThreadTapscatalogue195-231.pdf'
    },
    {
      img: '../../../assets/icon/Taps.png',
      name: 'Taps',
      code: 'ThreadingSolution',
      video : '../../../assets/videos/drill.mp4',
      share : true,
      ProductSubCategory	: 'Hand Taps,Machine Taps and Nut Taps',
      localpdf: '../../../assets/pdf/GroundThreadTapscatalogue195-231.pdf',
      onlinepdf: 'https://ecatalogue.addisonreach.co.in/Admin/brochurenew/GroundThreadTapscatalogue195-231.pdf'
    },
    {
      img: '../../../assets/icon/End Mills.png',
      name: 'Endmills',
      code: 'SurfacePreperation',
      video : '../../../assets/videos/drill.mp4',
      share : true,
      ProductSubCategory	: 'Slot Drill,Roughing End Mill,Screwed Shank End Mills',
      localpdf: '../../../assets/pdf/MillingCutter&Endmillcatalogue109-193.pdf',
      onlinepdf: 'https://ecatalogue.addisonreach.co.in/Admin/brochurenew/MillingCutter&Endmillcatalogue109-193.pdf'
    },
    {
      img: '../../../assets/icon/Cutters.png',
      name: 'Cutters',
      code: 'SurfacePreperation',
      video : '../../../assets/videos/drill.mp4',
      share : true,
      ProductSubCategory	: 'Convex Cutter,Concave cutter,Side&Face Cutter,Angle Cutter,Metal Slitting Saw',
      localpdf: '../../../assets/pdf/MillingCutter&Endmillcatalogue109-193.pdf',
      onlinepdf: 'https://ecatalogue.addisonreach.co.in/Admin/brochurenew/MillingCutter&Endmillcatalogue109-193.pdf'
    },
    {
      img: '../../../assets/icon/Drills.png',
      name: 'Slot Drills',
      code: 'SurfacePreperation',
      video : '../../../assets/videos/drill.mp4',
      share : true
    },
    {
      img: '../../../assets/icon/InnovationProduct.png',
      name: 'Slot Drills',
      code: 'InnovationProduct',
      video : '../../../assets/videos/drill.mp4',
      share : false
    },
    {
      img: '../../../assets/icon/machines.png',
      name: 'Machines',
      code: 'Machines',
      video : '../../../assets/videos/drill.mp4',
      share : true
    }
  ];
  filteredProdJSON: any = [];
  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.querystring) {
        this.obj = JSON.parse(params.querystring);
        console.log(this.obj);
      }
    });

    this.filteredProdJSON = this.productsJSON.filter(element => {
      console.log(element);
      if (element.code === this.obj.code) {
        return element;
      }
    });

  }

  gotoproductDetails(obj) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        querystring: JSON.stringify(obj)
      }
    };
    // this.route.navigate([`/productdetails`], navigationExtras);
    this.route.navigate([`/drilldashboard`], navigationExtras);
  }

  gotoVideo() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        querystring: JSON.stringify(this.obj)
      }
    };
    this.route.navigate([`/video`], navigationExtras);
  }

  async openPDFFn(obj) {
    const modal = await this.modalCtrl.create({
      component: ViewpdfComponent,
      // cssClass: 'custom-modal',
      componentProps: {
        object: obj,
        title: obj.name
      }
    });
    modal.onWillDismiss().then(() => {
      // this.fab.nativeElement.classList.remove('animated', 'bounceOutLeft')
      // this.animateCSS('bounceInLeft');
    });
    modal.present();
  }


}
