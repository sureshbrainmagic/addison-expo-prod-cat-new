import { ViewpdfComponent } from './../../component/viewpdf/viewpdf.component';
import { ConfigService } from './../../services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { Platform, AlertController, ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  corporateBrochure: any = {
    img: '../../../assets/icon/brochure.png',
    name: 'Corporate Brochure',
    code: 'CorporateBrochure',
    video: '../../../assets/videos/drill.mp4',
    share: true,
    ProductSubCategory: 'PSTD,TSTD and Centre Drills',
    localpdf: '../../../assets/pdf/Twistdrillcatalogue.pdf',
    onlinepdf: 'https://ecatalogue.addisonreach.co.in/Admin/brochurenew/Twistdrillcatalogue.pdf'
  };
  corporateAv: any = {
    img: '../../../assets/icon/youtube.png',
    name: 'Corporate A/V',
    code: 'CorporateA/V',
    video: '../../../assets/videos/drill.mp4',
    share: true
  };

  row1: any = [{
    img: '../../../assets/icon/drill.png',
    name: 'Hole Making',
    code: 'HoleMaking',
    video: '../../../assets/videos/drill.mp4',
    share: true
  },
  {
    img: '../../../assets/icon/ThreadingSolution.png',
    name: 'Threading Solution',
    code: 'ThreadingSolution',
    video: '../../../assets/videos/drill.mp4',
    share: true
  },
  {
    img: '../../../assets/icon/SurfacePreperation.png',
    name: 'Surface Preperation',
    code: 'SurfacePreperation',
    video: '../../../assets/videos/drill.mp4',
    share: true
  }];

  row2: any = [{
    img: '../../../assets/icon/GearTools.png',
    name: 'Gear Tools',
    code: 'GearTools',
    video: '../../../assets/videos/drill.mp4',
    share: true
  },
  {
    img: '../../../assets/icon/InnovationProduct.png',
    name: 'Innovation Product',
    code: 'InnovationProduct',
    video: '../../../assets/videos/drill.mp4',
    share: false
  },
  {
    img: '../../../assets/icon/machines.png',
    name: 'Machines',
    code: 'Machines',
    video: '../../../assets/videos/drill.mp4',
    share: true
  }];

  row3: any = [{
    img: '../../../assets/icon/Precisiontools.png',
    name: 'Precision tools',
    code: 'Precisiontools',
    video: '../../../assets/videos/drill.mp4',
    share: true
  },
  {
    img: '../../../assets/icon/wheels.png',
    name: 'Wheels',
    code: 'Wheels',
    video: '../../../assets/videos/drill.mp4',
    share: true
  }];

  constructor(
    private platform: Platform,
    private router: Router,
    private modalCtrl: ModalController,
    public config: ConfigService,
    private alertController: AlertController,
    private route: Router
  ) {
    this.platform.backButton.subscribe(async () => {
      const modal = await this.modalCtrl.getTop();
      if (modal) {
        modal.dismiss();
        return;
      }
      if (this.router.isActive('/home', true) && this.router.url === '/home') {
        navigator['app'].exitApp();
        // this.exitFunction();
      }
    });
  }

  ngOnInit() {
  }

  gotoProductDetails(obj) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        querystring: JSON.stringify(obj)
      }
    };
    this.route.navigate([`/productlist`], navigationExtras);
  }

  gotoVideo(videoObj) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        querystring: JSON.stringify(videoObj)
      }
    };
    this.route.navigate([`/video`], navigationExtras);
  }

  async openPDFFn(pdfObj, pdftitle) {
    const modal = await this.modalCtrl.create({
      component: ViewpdfComponent,
      // cssClass: 'custom-modal',
      componentProps: {
        object: pdfObj,
        title: pdftitle
      }
    });
    modal.onWillDismiss().then(() => {
      // this.fab.nativeElement.classList.remove('animated', 'bounceOutLeft')
      // this.animateCSS('bounceInLeft');
    });
    modal.present();
  }


}

