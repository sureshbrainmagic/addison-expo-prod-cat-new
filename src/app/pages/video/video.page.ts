import { ConfigService } from './../../services/config/config.service';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { add } from 'cart-localstorage';

@Component({
  selector: 'app-video',
  templateUrl: './video.page.html',
  styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit {
  obj: any = {};
  constructor(
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private config: ConfigService,
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.querystring) {
        this.obj = JSON.parse(params.querystring);
        console.clear();
        console.log(this.obj);
      }
    });
  }

  ngOnInit() {
  }

  async shareFn(mode, msg) {
    const alert = await this.alertCtrl.create({
      header: msg,
      backdropDismiss: false,
      inputs: [
        {
          name: 'txtValue',
          type: 'text',
          placeholder: msg,
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          },
        },
        {
          text: 'Okay',
          handler: (alertData) => {
            console.log(alertData);
            if (alertData.txtValue !== '') {
              const d = new Date();
              const time = d.getTime();

              if (mode === 'whatsapp') {
                add({ id: time, name: this.obj.name, sharedDt: new Date(), price: 100, mobileno: alertData.txtValue, mode: 'whatsapp', email: '', sharedContent: 'Video' });
                this.whatsappShare(alertData.txtValue);
              } else {
                add({ id: time, name: this.obj.name, sharedDt: new Date(), price: 100, mobileno: '', mode: 'mail', email: alertData.txtValue, sharedContent: 'Video' });
                this.sendEmail(alertData.txtValue);
              }
            } else {
              this.config.toastFn(msg);
              return false;
            }
          },
        },
      ],
    });
    await alert.present();
  }

  whatsappShare(mobilenos) {
    const values = {
      mobileno: mobilenos,
      msg: this.obj.name,
      video: this.obj.video,
      img1: '', // 'https://addisontoolsonline.com/img/logo.jpg',
      pdf: ''
    };
    this.config.loader('Please Wait ...');
    this.config.whatsAppSMS(`sendMSGinWA`, values).subscribe((res) => {
      const response: any = res;
      console.log(response);
      if (response.result) {
        const result = response.data;
        if (result.status === 'success') {
          this.config.msgAlertFn(`Shared to your whatsapp`);
        }
      }
      else {
        // this.config.toastFn(`No details found`);
      }
      this.config.loaderDismiss();
    }, err => {
      this.config.loaderDismiss();
      console.log(err);
    });
  }

  sendEmail(mail) {
    const values = {
      mailid: mail,
      msg: `${this.obj.name} Drills Videos`,
      img1: '',
      pdf: '',
      video: this.obj.video,
      sharemode: 'Video'
    };
    this.config.loader('Please Wait ...');
    this.config.sendMail(values).subscribe((res) => {
      const response: any = res;
      console.log(response);
      if (response.result) {
      }
      else {
        // this.config.toastFn(`No details found`);
      }
      this.config.loaderDismiss();
    }, err => {
      this.config.loaderDismiss();
      console.log(err);
    });

  }

}
