import { ConfigService } from './../../services/config/config.service';
import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import { add } from 'cart-localstorage';
const ZOOM_STEP: number = 0.25;
const DEFAULT_ZOOM: number = 1;

@Component({
  selector: 'app-viewpdf',
  templateUrl: './viewpdf.component.html',
  styleUrls: ['./viewpdf.component.scss'],
})
export class ViewpdfComponent implements OnInit {
  obj;
  url;
  title = 'PDF';
  isShow: boolean = true;
  public pdfZoom: number = DEFAULT_ZOOM;

  constructor(
    private modalCtrl: ModalController,
    private navParams: NavParams,
    private config: ConfigService,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    console.log(this.navParams.data.object);
    this.obj = this.navParams.data.object;
    this.title = this.navParams.data.title;
    this.url = `${this.navParams.data.object.localpdf}`;
    console.log('Pdfurl', this.url)
    setTimeout(() => {
      this.isShow = false;
    }, 30000);
  }

  onProgress(e) {
    console.log('on-progress', e);
    this.isShow = false;
  }

  zoomIn() {
    this.pdfZoom += ZOOM_STEP;
  }

  zoomOut() {
    if (this.pdfZoom > DEFAULT_ZOOM) {
      this.pdfZoom -= ZOOM_STEP;
    }
  }

  resetZoom() {
    this.pdfZoom = DEFAULT_ZOOM;
  }

  close() {
    this.modalCtrl.dismiss();
  }

  async shareFn(mode, msg) {
    const alert = await this.alertCtrl.create({
      header: msg,
      backdropDismiss: false,
      inputs: [
        {
          name: 'txtValue',
          type: 'text',
          placeholder: msg,
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          },
        },
        {
          text: 'Okay',
          handler: (alertData) => {
            console.log(alertData);
            const d = new Date();
            const time = d.getTime();

            if (alertData.txtValue !== '') {
              if (mode === 'whatsapp') {
                this.whatsappShare(alertData.txtValue);
                add({ id: time, name: this.obj.name, sharedDt: new Date(), price: 100, mobileno: alertData.txtValue, mode: 'whatsapp', email: '', sharedContent: 'Brochure' });
              } else {
                add({ id: time, name: this.obj.name, sharedDt: new Date(), price: 100, mobileno: '', mode: 'mail', email: alertData.txtValue, sharedContent: 'Brochure' });
                this.sendEmail(alertData.txtValue);
              }
            } else {
              this.config.toastFn(msg);
              return false;
            }
          },
        },
      ],
    });
    await alert.present();
  }

  whatsappShare(mobilenos) {
    const values = {
      mobileno: mobilenos,
      msg: this.navParams.data.object.name,
      video: '',
      img1: '', // 'https://addisontoolsonline.com/img/logo.jpg',
      pdf: this.navParams.data.object.onlinepdf
    };
    this.config.loader('Please Wait ...');
    this.config.whatsAppSMS(`sendMSGinWA`, values).subscribe((res) => {
      const response: any = res;
      console.log(response);
      if (response.result) {
        const result = response.data;
        if (result.status === 'success') {
          this.config.msgAlertFn(`Shared to your whatsapp`);
        }
      }
      else {
        // this.config.toastFn(`No details found`);
      }
      this.config.loaderDismiss();
    }, err => {
      this.config.loaderDismiss();
      console.log(err);
    });
  }

  sendEmail(mail) {
    const values = {
      mailid: mail,
      msg: this.navParams.data.object.name,
      img1: '',
      pdf: this.navParams.data.object.onlinepdf,
      video: '',
      sharemode: this.navParams.data.object.name + ' Brochure'
    };
    this.config.loader('Please Wait ...');
    this.config.sendMail(values).subscribe((res) => {
      const response: any = res;
      console.log(response);
      if (response.result) {
      }
      else {
      }
      this.config.loaderDismiss();
    }, err => {
      this.config.loaderDismiss();
      console.log(err);
    });

  }


}
