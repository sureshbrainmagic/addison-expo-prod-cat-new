import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { ViewpdfComponent } from './viewpdf/viewpdf.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    ViewpdfComponent
  ],
  imports: [
    IonicModule,
    FormsModule,
    LazyLoadImageModule,
    PdfViewerModule,
    CommonModule
  ],
  exports: [
    ViewpdfComponent
  ]
})
export class ComponentsModule { }
