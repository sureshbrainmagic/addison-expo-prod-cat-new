import { Component } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { MenuController, Platform } from '@ionic/angular';
import { ConfigService } from './services/config/config.service';
import { NavigationExtras, Router } from '@angular/router';
// declare var window;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  lsUserDetails: any = [];
  public appPages = [
    { title: 'Home', url: '/dashboard', icon: 'home' },
    { title: 'Customers', url: '/viewcustomer', icon: 'people' },
    { title: 'View Attendance', url: '/viewattendances', icon: 'calendar' },
    { title: 'Visit Reports', url: '/reportview', icon: 'location' },
    { title: 'Profile', url: '/profile', icon: 'person' },
    { title: 'Change Password', url: '/changepwd', icon: 'lock-closed' },
    { title: 'Contact Us', url: '/contactus', icon: 'call' },
  ];
  constructor(
    private network: Network,
    public config: ConfigService,
    public platform: Platform,
    private statusBar: StatusBar,
    private screenOrientation: ScreenOrientation,
    private router: Router,
    private menuCtrl: MenuController
  ) {
    this.platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);
      this.menuCtrl.enable(false);
      this.statusBar.backgroundColorByHexString('#2684cc');
      // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      // this.network.onConnect().subscribe(() => {
      //   setTimeout(() => {
      //     if (this.network.type === 'wifi') {
      //       console.log('we got a wifi connection, woohoo!');
      //     }
      //   }, 3000);
      // });
      // this.network.onDisconnect().subscribe(() => {
      //   this.config.exitFunction('Exit and try again', 'Internet is not available ...!');
      //   this.config.toastFn(`Internet is not available`);
      // });
      // window.app = this;
    });
  }

  callmethod() {
    this.lsUserDetails = [];
    if (localStorage.getItem('lsautoserwal') != null) {
      this.lsUserDetails = JSON.parse(localStorage.getItem('localStorage'));
      console.log(this.lsUserDetails);
    }
  }

}
