import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { ConfigService } from '../config/config.service';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(
    // private http: HttpClient,
    private config: ConfigService,
    private httpBackend: HttpBackend
  ) { }

  // uploadFile(formData) {
  //   console.log(formData);
  //   // ${this.config.rootUrl}
  //   return this.http.post(`${this.config.rootUrl}/Upload/PostImage`, formData);
  // }

  uploadFile(formData, foldername) {
    console.log(formData);
    const newHttpClient = new HttpClient(this.httpBackend);
    // return newHttpClient.post(`${this.config.uploadImgUrl}/PostImage?foldername=${foldername}`, formData);
    return newHttpClient.post(`${this.config.uploadImgUrl}/PostImage`, formData);
  }

}
