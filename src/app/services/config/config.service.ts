import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController, LoadingController, ToastController, Platform, ModalController } from '@ionic/angular';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CallNumber } from '@ionic-native/call-number/ngx';
@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  public noImg = '../../../assets/icon/visitingcard.png';
  public noProfile = '../../../assets/icon/profile.png';
  public logo = 'https://addisontoolsonline.com/img/logo.jpg';

  public whatsappUrl = `http://122.165.54.203:8001/addisonexpoapi/api/Whatsapp/`;

  public rootUrl = 'http://122.165.54.203:8001/addisonexpoapi/api/Values/';
  public uploadImgUrl = 'http://122.165.54.203:8001/addisonexpoapi/api/upload/';
  public imgRootUrl = 'http://122.165.54.203:8001/addisonexpoapi/img/';

  isLoading = false;
  constructor(
    private http: HttpClient,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertController: AlertController,
    private location: Location,
    private router: Router,
    private platform: Platform,
    private route: Router,
    private modalCtrl: ModalController,
    private callNumber: CallNumber,
  ) { }

  getData(url): Observable<any> {
    const geturl = `${this.rootUrl}${url}`;
    return this.http.get(geturl);
  }

  postData(url, bodyValues): Observable<any> {
    const apiURL = `${this.rootUrl}${url}`;
    return this.http.post(apiURL, bodyValues);
  }

  sendMail(bodyValues): Observable<any> {
    const apiURL = `${this.rootUrl}sendmail`;
    return this.http.post(apiURL, bodyValues);
  }

  whatsAppSMS(url, bodyValues): Observable<any> {
    const apiURL = `${this.whatsappUrl}${url}`;
    return this.http.post(apiURL, bodyValues);
  }


  isLogin(): string {
    return localStorage.getItem('isLogin');
  }

  lsUserDetails() {
    return JSON.parse(localStorage.getItem('lsfieldStaffUserDetail'));
  }

  hardwareBackbtn() {
    this.platform.backButton.subscribe((async) => {
      this.location.back();
    });
  }

  backFn() {
    this.location.back();
  }

  inrCurrency(x) {
    const currency = new Intl.NumberFormat('en-IN', { currency: 'INR' }).format(x);
    // currency: 'INR': 'symbol-narrow': '2.2-2'
    return '₹ ' + currency;
  }

  // openInAppBrowser(url) {
  //   const options: InAppBrowserOptions = {
  //     location: 'yes',//Or 'no'
  //     hidden: 'no', //Or  'yes'
  //     clearcache: 'yes',
  //     clearsessioncache: 'yes',
  //     zoom: 'yes',//Android only ,shows browser zoom controls
  //     hardwareback: 'yes',
  //     shouldPauseOnSuspend: 'no', //Android only
  //     fullscreen: 'yes',//Windows only
  //   };
  //   // const browser = this.iab.create(url);
  //   const browser = this.iab.create(url, '_system', options);
  // }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  async callNumberFn(no) {
    const alert = await this.alertController.create({
      header: 'Call',
      message: `Do you want to call ?`,
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Yes',
          handler: () => {
            this.callNumber
              .callNumber(no, true)
              .then((res) => console.log('Launched dialer!', res))
              .catch((err) => console.log('Error launching dialer', err));
          },
        },
      ],
    });
    await alert.present();
  }

  timeFn(time: any): any {
    let hour = (time.split(':'))[0];
    let min = (time.split(':'))[1];
    const part = hour > 12 ? 'PM' : 'AM';
    if (parseInt(hour) === 0)
      hour = 12;
    min = (min + '').length === 1 ? `0${min}` : min;
    hour = hour > 12 ? hour - 12 : hour;
    hour = (hour + '').length === 1 ? `0${hour}` : hour;
    return `${hour}:${min} ${part}`;
  }

  async loader(msg) {
    this.isLoading = true;
    return await this.loadingCtrl.create({
      spinner: 'crescent',
      message: msg,
      duration: 20000,
    }).then(a => {
      a.present().then(() => {
        // console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => {
            console.log('abort presenting');
          });
        }
      });
    });
  }

  async loaderDismiss() {
    this.isLoading = false;
    return await this.loadingCtrl.dismiss().then(() => {
      console.log('dismissed');
    });
  }

  async toastFn(msg: string, positiontxt: any = 'bottom', headerString?: string) {
    const toast = await this.toastCtrl.create({
      header: headerString,
      message: msg,
      position: positiontxt,
      duration: 3000,
      cssClass: 'toastcustom',
      buttons: [
        {
          text: 'ok',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await toast.present();
  }


  async goToLogin(msg) {
    const alert = await this.alertController.create({
      header: 'Information!',
      message: msg,
      buttons: [
        {
          text: 'Login',
          handler: () => {
            // this.router.navigateByUrl('/enquiry');
          }
        },
        {
          text: 'Close',
          role: 'cancel',
          handler: () => {
            this.location.back();
            console.log('Confirm Cancel: blah');
          }
        },
      ]
    });
    await alert.present();
  }



  async msgAlertFn(msg) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        },
      ]
    });
    await alert.present();
  }

  async exitFunction(heading, msg: string) {
    const alert = await this.alertController.create({
      header: heading,
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            navigator['app'].exitApp();
            // console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

  async exitApp() {
    const alert = await this.alertController.create({
      // header: 'Confirm!',
      message: 'Are you sure want to Exit?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Yes',
          handler: () => {
            console.log('Confirm Okay');
            // navigator['app'].exitApp();
            // navigator.app.exitApp();
          }
        }
      ]
    });

    await alert.present();
  }

  async exitInternetFn(heading, msg: string) {
    const alert = await this.alertController.create({
      header: heading,
      message: msg,
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            navigator['app'].exitApp();
            // console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }
  async logoutFn() {
    const alert = await this.alertController.create({
      header: 'Logout ?',
      message: 'Do you want to logout the app?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {

          }
        },
        {
          text: 'Logout',
          handler: () => {
            // this.menuCtrl.close();
            // this.menuCtrl.enable(false);
            localStorage.clear();
            this.route.navigate(['/login']);
          }
        }
      ]
    });
    await alert.present();
  }
}
