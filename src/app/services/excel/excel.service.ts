import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import { File } from '@ionic-native/file/ngx';
import { ConfigService } from './../config/config.service';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor(
    private file: File,
    private config: ConfigService
  ) { }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    console.log('worksheet', worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    const xlfilename = fileName + new Date().getTime() + EXCEL_EXTENSION;
    this.file.checkDir(this.file.externalRootDirectory, 'Addison')
      .then(_ => {
        this.file.writeFile(this.file.externalRootDirectory + 'Addison/', xlfilename, data).then(response => {
          // ACTION
          console.log('40',response);
          this.config.toastFn(`Downloaded successfully at Addison Folder`);
        }).catch(err => {
          // ACTION
          console.log('44',err);
        })
      })
      .catch(err => {
        this.file.createDir(this.file.externalRootDirectory, 'Addison', false).then(result => {
          this.file.writeFile(this.file.externalRootDirectory + 'Addison/', xlfilename, data).then(response => {
            // ACTION
            console.log('51',response);
            this.config.toastFn(`Downloaded successfully at Addison Folder`);
          }).catch(err => {
            // ACTION
            console.log('54',err);
          })
        })
      });
  }
}
